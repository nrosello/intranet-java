package com.adistec.intranet.service;

import java.io.IOException;
import java.net.URISyntaxException;

import org.springframework.web.multipart.MultipartFile;

import com.adistec.intranet.dto.ContactFormLogDTO;

public interface ContactFormLogService {

	ContactFormLogDTO getById(Integer id);
	ContactFormLogDTO saveAndFlush(ContactFormLogDTO contactFormLogDTO, MultipartFile file) throws IOException, URISyntaxException;
}
