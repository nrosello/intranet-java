package com.adistec.intranet.service;

import java.util.List;

import com.adistec.intranet.dto.AreaDTO;

public interface AreaService {
	List<AreaDTO> getAll();
}
