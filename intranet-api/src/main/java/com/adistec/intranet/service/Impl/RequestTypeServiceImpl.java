package com.adistec.intranet.service.Impl;

import java.util.List;
import java.util.stream.Collectors;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import com.adistec.intranet.dto.RequestTypeDTO;
import com.adistec.intranet.entity.mysql.RequestType;
import com.adistec.intranet.mapper.RequestTypeMapper;
import com.adistec.intranet.repository.mysql.RequestTypeRepository;
import com.adistec.intranet.service.RequestTypeService;

@Service
@Transactional("mysqlTransactionManager")
public class RequestTypeServiceImpl implements RequestTypeService {

	@Autowired
	private RequestTypeRepository requestTypeRepository;

	@Autowired
	private RequestTypeMapper mapper;

	@Override
	public List<RequestTypeDTO> getAll() {
		List<RequestType> entities = requestTypeRepository.findAll();
		return entities.stream().map(entity -> mapper.toRequestTypeDTO(entity)).collect(Collectors.toList());
	}

	@Override
	public RequestTypeDTO getById(Integer id) {
		RequestType requestType = requestTypeRepository.findById(id);
		return mapper.toRequestTypeDTO(requestType);
	}

}
