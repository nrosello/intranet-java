package com.adistec.intranet.service.Impl;

import java.util.List;
import java.util.stream.Collectors;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import com.adistec.intranet.dto.AreaDTO;
import com.adistec.intranet.entity.mysql.Area;
import com.adistec.intranet.repository.mysql.AreaRepository;
import com.adistec.intranet.service.AreaService;

@Service
@Transactional("mysqlTransactionManager")
public class AreaServiceImpl implements AreaService {

	@Autowired
	private AreaRepository areaRepository;

	@Override
	public List<AreaDTO> getAll() {
		List<Area> entities = areaRepository.findAll();
		return entities.stream().map(entity -> new AreaDTO(entity.getId(), entity.getName()))
				.collect(Collectors.toList());
	}

}
