package com.adistec.intranet.service;

import java.io.IOException;

import org.springframework.web.multipart.MultipartFile;

import com.adistec.intranet.dto.HelpRequestDTO;

public interface HelpRequestService {
	HelpRequestDTO getById(Integer id);

	HelpRequestDTO saveAndFlush(HelpRequestDTO helpRequest, MultipartFile[] files) throws IOException;
}
