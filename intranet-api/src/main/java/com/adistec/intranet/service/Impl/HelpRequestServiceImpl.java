package com.adistec.intranet.service.Impl;

import java.io.File;
import java.io.IOException;
import java.net.URISyntaxException;
import java.util.ArrayList;
import java.util.Date;
import java.util.HashMap;
import java.util.Map;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.core.io.ClassPathResource;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;
import org.springframework.web.multipart.MultipartFile;
import com.adistec.intranet.dto.AttachmentDTO;
import com.adistec.intranet.dto.HelpRequestDTO;
import com.adistec.intranet.entity.mysql.HelpRequest;
import com.adistec.intranet.mapper.HelpRequestMapper;
import com.adistec.intranet.repository.mysql.HelpRequestRepository;
import com.adistec.intranet.service.HelpRequestService;
import com.adistec.intranet.util.FileUtils;
import com.adistec.mailclient.impl.MailClient;

@Service
@Transactional("mysqlTransactionManager")
public class HelpRequestServiceImpl implements HelpRequestService {

	private final Logger LOGGER = LoggerFactory.getLogger(HelpRequestServiceImpl.class);

	@Autowired
	private HelpRequestRepository helpRequestRepository;

	@Autowired
	private HelpRequestMapper mapper;

	@Autowired
	private MailClient mailClient;

	@Value("${files.upload.directory}")
	private String filesDirectory;

	public HelpRequestDTO getById(Integer id) {
		return mapper.toHelpRequestDTO(helpRequestRepository.findById(id));
	}

	public HelpRequestDTO saveAndFlush(HelpRequestDTO helpRequestDTO, MultipartFile[] files) throws IOException {
		helpRequestDTO.setDate(new Date());
		try {
			LOGGER.info("Sending help request to  " + helpRequestDTO.getDeliverto());
			saveAttachments(files, helpRequestDTO);
			String attachmentsString = setAttachmentsString(helpRequestDTO);
			Map<String, String> vars = new HashMap<String, String>();
			setTemplateVariables(helpRequestDTO, attachmentsString, vars);
			String[] to = new String[] { helpRequestDTO.getDeliverto() };
			File template = new ClassPathResource("templates/intranet.html").getFile();
			String subject = helpRequestDTO.getRequesttype() + " <" + helpRequestDTO.getEmail() + ">";
			mailClient.sendEmailWithTemplate(to, template, vars, subject, null);
			LOGGER.info(
					"Help request from " + helpRequestDTO.getEmail() + " Sent to " + to + " with subject " + subject);
			helpRequestDTO.setDelivered(true);
		} catch (URISyntaxException | IOException e) {
			LOGGER.error("Error sending help request exception message: " + e.getMessage());
			helpRequestDTO.setDelivered(false);
		}
		HelpRequest savedEntity = mapper.toHelpRequest(helpRequestDTO);
		savedEntity = helpRequestRepository.saveAndFlush(savedEntity);
		return mapper.toHelpRequestDTO(savedEntity);
	}

	private void setTemplateVariables(HelpRequestDTO helpRequestDTO, final String attachmentsString,
			Map<String, String> vars) {
		vars.put("name", helpRequestDTO.getName());
		vars.put("email", helpRequestDTO.getEmail());
		vars.put("subsidiary", helpRequestDTO.getSubsidiary());
		vars.put("message", helpRequestDTO.getMessage());
		vars.put("links", attachmentsString);
	}

	private String setAttachmentsString(HelpRequestDTO helpRequestDTO) {
		String attachments = "";
		int acc = 0;
		for (AttachmentDTO attachment : helpRequestDTO.getAttachments()) {
			acc += 1;
			attachments += "Link to File " + acc + ": " + attachment.getAttachmentname() + " #NL";
		}
		return attachments;
	}

	private void saveAttachments(MultipartFile[] files, HelpRequestDTO helpRequestDTO) throws IOException {
		if (helpRequestDTO.getAttachments() == null) {
			helpRequestDTO.setAttachments(new ArrayList<AttachmentDTO>());
		}
		for (MultipartFile file : files) {
			String fileName = FileUtils.uploadFile(filesDirectory, file);
			addAttachmentToHelpRequest(helpRequestDTO, fileName);

		}
	}

	private void addAttachmentToHelpRequest(HelpRequestDTO helpRequestDTO, String fileName) {
		AttachmentDTO dto = new AttachmentDTO();
		dto.setAttachmentname(fileName);
		helpRequestDTO.getAttachments().add(dto);
	}

}
