package com.adistec.intranet.service.Impl;

import java.io.File;
import java.io.IOException;
import java.net.URISyntaxException;
import java.util.Date;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.core.io.ClassPathResource;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;
import org.springframework.web.multipart.MultipartFile;
import com.adistec.intranet.dto.ContactFormLogDTO;
import com.adistec.intranet.entity.common.IContactFormLog;
import com.adistec.intranet.entity.mysql.ContactFormLogMysql;
import com.adistec.intranet.entity.sqlserver.ContactFormLogSqlServer;
import com.adistec.intranet.mapper.ContactFormLogMapper;
import com.adistec.intranet.repository.mysql.ContactFormLogMysqlRepository;
import com.adistec.intranet.repository.sqlserver.ContactFormLogSqlServerRepository;
import com.adistec.intranet.security.SecurityContextUtil;
import com.adistec.intranet.service.ContactFormLogService;
import com.adistec.intranet.util.FileUtils;
import com.adistec.mailclient.impl.MailClient;

@Service
public class ContactFormLogServiceImpl implements ContactFormLogService {

	private static final Logger LOG = LoggerFactory.getLogger(ContactFormLogServiceImpl.class);

	@Autowired
	private ContactFormLogMysqlRepository contactFormLogMysqlRepository;

	@Autowired
	private ContactFormLogSqlServerRepository contactFormLogSqlServerRepository;

	@Autowired
	private ContactFormLogMapper mapper;

	@Autowired
	private MailClient mailClient;

	@Value("${referral.source}")
	private String referralSource;

	@Value("${mail.referral.subject}")
	private String referralSubject;

	@Value("${files.upload.directory}")
	private String filesDirectory;

	@Transactional("sqlserverTransactionManager")
	public ContactFormLogDTO getById(Integer id) {
		return mapper.toContactFormLogDTO(contactFormLogSqlServerRepository.findById(id));
	}

	public ContactFormLogDTO saveAndFlush(ContactFormLogDTO contactFormLogDTO, MultipartFile file)
			throws IOException, URISyntaxException {
		String sender = SecurityContextUtil.getCustomProperty(SecurityContextUtil.USER_NAME);
		contactFormLogDTO.setSource(referralSource);
		contactFormLogDTO.setDateCreated(new Date());
		contactFormLogDTO.setEmailReference(sender);
		String uploadedFile = FileUtils.uploadFile(filesDirectory, file);
		contactFormLogDTO.setUploadedFile(uploadedFile);
		ContactFormLogMysql contactFormLogMysql = mapper.toContactFormLogMysql(contactFormLogDTO);
		ContactFormLogSqlServer contactFormLogSqlServer = mapper.toContactFormLogSqlServer(contactFormLogDTO);
		saveMysql(contactFormLogMysql);
		ContactFormLogDTO savedDTO = mapper.toContactFormLogDTO(saveSqlServer(contactFormLogSqlServer));
		sendMailToSender(new String[] { sender });
		sendMailToReferred(new String[] { contactFormLogDTO.getEmail() });
		return savedDTO;
	}

	@Transactional("sqlserverTransactionManager")
	private IContactFormLog saveSqlServer(ContactFormLogSqlServer entity) {
		LOG.info("Saving contact form log in sql server db");
		IContactFormLog savedEntity = contactFormLogSqlServerRepository.saveAndFlush(entity);
		LOG.info("Contact form log with id: " + savedEntity.getId() + " saved in sql server db");
		return savedEntity;
	}

	@Transactional("mysqlTransactionManager")
	private void saveMysql(ContactFormLogMysql entity) {
		LOG.info("Saving contact form log in mysql db");
		IContactFormLog savedEntity = contactFormLogMysqlRepository.saveAndFlush(entity);
		LOG.info("Contact form log with id: " + savedEntity.getId() + " saved in mysql db");
	}

	private void sendMailToReferred(String[] to) throws URISyntaxException, IOException {
		File template = new ClassPathResource("templates/referred.html").getFile();
		sendMail(to, template, referralSubject);
	}

	private void sendMailToSender(String[] to) throws URISyntaxException, IOException {
		File template = new ClassPathResource("templates/sender.html").getFile();
		sendMail(to, template, referralSubject);
	}

	private void sendMail(String[] to, File template, String subject) throws URISyntaxException, IOException {
		mailClient.sendEmailWithTemplate(to, template, null, subject, null, "Adistec <no-reply@adistec.com>");
	}
}
