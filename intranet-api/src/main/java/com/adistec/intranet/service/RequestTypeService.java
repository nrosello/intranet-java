package com.adistec.intranet.service;

import java.util.List;

import com.adistec.intranet.dto.RequestTypeDTO;

public interface RequestTypeService {

	public List<RequestTypeDTO> getAll();

	public RequestTypeDTO getById(Integer id);
}
