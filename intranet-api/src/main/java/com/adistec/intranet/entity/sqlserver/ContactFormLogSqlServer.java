package com.adistec.intranet.entity.sqlserver;

import java.util.Date;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.Table;

import com.adistec.intranet.entity.common.IContactFormLog;

@Entity
@Table(name = "ContactFormLog")
public class ContactFormLogSqlServer implements IContactFormLog {

	@Id
	@GeneratedValue(strategy = GenerationType.IDENTITY)
	private Integer id;

	@Column(name = "firstname", columnDefinition = "nvarchar")
	private String firstName;

	@Column(name = "lastname", columnDefinition = "nvarchar")
	private String lastName;

	@Column(name = "email", columnDefinition = "nvarchar")
	private String email;

	@Column(name = "phonenumber", columnDefinition = "nvarchar")
	private String phone;

	@Column(name = "country", columnDefinition = "nvarchar")
	private String country;

	@Column(name = "datecreated", columnDefinition = "datetime")
	private Date dateCreated;

	@Column(name = "source", columnDefinition = "nvarchar")
	private String source;

	@Column(name = "position", columnDefinition = "nvarchar")
	private String position;

	@Column(name = "countrysource", columnDefinition = "varchar")
	private String countrySource;

	@Column(name = "uploadedfile", columnDefinition = "varchar")
	private String uploadedFile;

	@Column(name = "emailreference", columnDefinition = "varchar")
	private String emailReference;

	@Column(name = "typereferred", columnDefinition = "varchar")
	private String typeReferred;

	@Column(name = "searchcomments", columnDefinition = "varchar")
	private String searchComments;

	public Integer getId() {
		return id;
	}

	public void setId(Integer id) {
		this.id = id;
	}

	public String getFirstName() {
		return firstName;
	}

	public void setFirstName(String firstName) {
		this.firstName = firstName;
	}

	public String getLastName() {
		return lastName;
	}

	public void setLastName(String lastName) {
		this.lastName = lastName;
	}

	public String getEmail() {
		return email;
	}

	public void setEmail(String email) {
		this.email = email;
	}

	public String getPhone() {
		return phone;
	}

	public void setPhone(String phone) {
		this.phone = phone;
	}

	public String getCountry() {
		return country;
	}

	public void setCountry(String country) {
		this.country = country;
	}

	public Date getDateCreated() {
		return dateCreated;
	}

	public void setDateCreated(Date dateCreated) {
		this.dateCreated = dateCreated;
	}

	public String getSource() {
		return source;
	}

	public void setSource(String source) {
		this.source = source;
	}

	public String getPosition() {
		return position;
	}

	public void setPosition(String position) {
		this.position = position;
	}

	public String getCountrySource() {
		return countrySource;
	}

	public void setCountrySource(String countrySource) {
		this.countrySource = countrySource;
	}

	public String getUploadedFile() {
		return uploadedFile;
	}

	public void setUploadedFile(String uploadedFile) {
		this.uploadedFile = uploadedFile;
	}

	public String getEmailReference() {
		return emailReference;
	}

	public void setEmailReference(String emailReference) {
		this.emailReference = emailReference;
	}

	public String getTypeReferred() {
		return typeReferred;
	}

	public void setTypeReferred(String typeReferred) {
		this.typeReferred = typeReferred;
	}

	public String getSearchComments() {
		return searchComments;
	}

	public void setSearchComments(String searchComments) {
		this.searchComments = searchComments;
	}

}
