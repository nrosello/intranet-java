package com.adistec.intranet.entity.mysql;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.Id;
import javax.persistence.Table;

@Entity
@Table(name = "request_type")
public class RequestType {

	@Id
	@Column(name = "id")
	@GeneratedValue
	private Integer id;

	@Column(name = "subject", columnDefinition = "nvarchar")
	private String subject;

	@Column(name = "deliverto", columnDefinition = "nvarchar")
	private String deliverto;

	@Column(name = "delivercc", columnDefinition = "nvarchar")
	private String delivercc;

	@Column(name = "deliverbcc", columnDefinition = "nvarchar")
	private String deliverbcc;

	public Integer getId() {
		return id;
	}

	public void setId(Integer id) {
		this.id = id;
	}

	public String getSubject() {
		return subject;
	}

	public void setSubject(String subject) {
		this.subject = subject;
	}

	public String getDeliverto() {
		return deliverto;
	}

	public void setDeliverto(String deliverto) {
		this.deliverto = deliverto;
	}

	public String getDelivercc() {
		return delivercc;
	}

	public void setDelivercc(String delivercc) {
		this.delivercc = delivercc;
	}

	public String getDeliverbcc() {
		return deliverbcc;
	}

	public void setDeliverbcc(String deliverbcc) {
		this.deliverbcc = deliverbcc;
	}

}
