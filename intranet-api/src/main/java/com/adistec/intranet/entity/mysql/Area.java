package com.adistec.intranet.entity.mysql;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.Id;
import javax.persistence.Table;

@Entity
@Table(name = "areas")
public class Area {

	@Id
	@GeneratedValue
	@Column(name = "id", columnDefinition = "nvarchar")
	private String id;

	@Column(name = "name", columnDefinition = "nvarchar")
	private String name;

	@Column(name = "internal_id", columnDefinition = "nvarchar")
	private String internalId;

	public String getId() {
		return id;
	}

	public void setId(String id) {
		this.id = id;
	}

	public String getName() {
		return name;
	}

	public void setName(String name) {
		this.name = name;
	}

	public String getInternalId() {
		return internalId;
	}

	public void setInternalId(String internalId) {
		this.internalId = internalId;
	}

}
