package com.adistec.intranet.entity.mysql;

import java.util.Date;
import java.util.List;

import javax.persistence.CascadeType;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.OneToMany;
import javax.persistence.Table;

@Entity
@Table(name = "help_request")
public class HelpRequest {

	@Id
	@GeneratedValue(strategy = GenerationType.IDENTITY)
	private Integer id;

	@Column(name = "date", columnDefinition = "datetime")
	private Date date;

	@Column(name = "ip", columnDefinition = "nvarchar")
	private String ip;

	@Column(name = "name", columnDefinition = "nvarchar")
	private String name;

	@Column(name = "email", columnDefinition = "nvarchar")
	private String email;

	@Column(name = "requesttype", columnDefinition = "nvarchar")
	private String requesttype;

	@Column(name = "message", columnDefinition = "nvarchar")
	private String message;

	@OneToMany(cascade=CascadeType.ALL)
	@JoinColumn(name="help_request_id")
	private List<Attachment> attachments;

	@Column(name = "deliverto", columnDefinition = "nvarchar")
	private String deliverto;

	@Column(name = "delivercc", columnDefinition = "nvarchar")
	private String delivercc;

	@Column(name = "deliverbcc", columnDefinition = "nvarchar")
	private String deliverbcc;

	@Column(name = "delivered", columnDefinition = "bit")
	private Boolean delivered;

	@Column(name = "subsidiary", columnDefinition = "nvarchar")
	private String subsidiary;

	public Integer getId() {
		return id;
	}

	public void setId(Integer id) {
		this.id = id;
	}

	public String getName() {
		return name;
	}

	public void setName(String name) {
		this.name = name;
	}

	public String getRequesttype() {
		return requesttype;
	}

	public void setRequesttype(String requesttype) {
		this.requesttype = requesttype;
	}

	public List<Attachment> getAttachments() {
		return attachments;
	}

	public void setAttachments(List<Attachment> attachments) {
		this.attachments = attachments;
	}

	public String getDeliverto() {
		return deliverto;
	}

	public void setDeliverto(String deliverto) {
		this.deliverto = deliverto;
	}

	public String getDelivercc() {
		return delivercc;
	}

	public void setDelivercc(String delivercc) {
		this.delivercc = delivercc;
	}

	public Boolean getDelivered() {
		return delivered;
	}

	public void setDelivered(Boolean delivered) {
		this.delivered = delivered;
	}

	public String getSubsidiary() {
		return subsidiary;
	}

	public void setSubsidiary(String subsidiary) {
		this.subsidiary = subsidiary;
	}

	public Date getDate() {
		return date;
	}

	public void setDate(Date date) {
		this.date = date;
	}

	public String getIp() {
		return ip;
	}

	public void setIp(String ip) {
		this.ip = ip;
	}

	public String getMessage() {
		return message;
	}

	public void setMessage(String message) {
		this.message = message;
	}

	public String getEmail() {
		return email;
	}

	public void setEmail(String email) {
		this.email = email;
	}

	public String getDeliverbcc() {
		return deliverbcc;
	}

	public void setDeliverbcc(String deliverbcc) {
		this.deliverbcc = deliverbcc;
	}

}
