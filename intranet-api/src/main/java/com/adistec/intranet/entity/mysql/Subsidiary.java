package com.adistec.intranet.entity.mysql;

public class Subsidiary {

	private long id;
	private String name;
	private String quoteMessage;
	private String city;
	private String state;
	private String country;

	public long getId() {
		return id;
	}

	public String getName() {
		return name;
	}

	public String getQuoteMessage() {
		return quoteMessage;
	}

	public String getCity() {
		return city;
	}

	public String getState() {
		return state;
	}

	public String getCountry() {
		return country;
	}

	public void setId(long id) {
		this.id = id;
	}

	public void setName(String name) {
		this.name = name;
	}

	public void setQuoteMessage(String quoteMessage) {
		this.quoteMessage = quoteMessage;
	}

	public void setCity(String city) {
		this.city = city;
	}

	public void setState(String state) {
		this.state = state;
	}

	public void setCountry(String country) {
		this.country = country;
	}

}
