package com.adistec.intranet.controller;

import java.util.stream.Collectors;

import org.springframework.http.ResponseEntity;
import org.springframework.security.core.context.SecurityContextHolder;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RestController;

import com.adistec.intranet.dto.UserDTO;
import com.adistec.intranet.security.SecurityContextUtil;

@RestController
public class UserController {

	@GetMapping("/user/me")
	public ResponseEntity<?> currentUser() {
		return ResponseEntity.ok(
				new UserDTO.Builder().firstName(SecurityContextUtil.getCustomProperty(SecurityContextUtil.FIRST_NAME))
						.lastName(SecurityContextUtil.getCustomProperty(SecurityContextUtil.LAST_NAME))
						.userName(SecurityContextUtil.getCustomProperty(SecurityContextUtil.USER_NAME))
						.locale(SecurityContextUtil.getCustomProperty(SecurityContextUtil.LOCALE))
						.roles(SecurityContextHolder.getContext().getAuthentication().getAuthorities().stream()
								.map(role -> role.getAuthority()).collect(Collectors.toList()))
						.permissions(SecurityContextUtil.getCustomList(SecurityContextUtil.PERMISSIONS)).build());
	}

}
