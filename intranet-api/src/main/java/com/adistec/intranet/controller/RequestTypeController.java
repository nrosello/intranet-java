package com.adistec.intranet.controller;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RestController;

import com.adistec.intranet.service.RequestTypeService;

@RestController
@RequestMapping(value = "/requestType")
public class RequestTypeController {

	private static final Logger LOG = LoggerFactory.getLogger(HelpRequestController.class);

	@Autowired
	private RequestTypeService requestTypeService;

	@RequestMapping(value = "/{id}", method = RequestMethod.GET)
	public ResponseEntity<?> getByid(@PathVariable Integer id) {
		try {
			LOG.info("Getting RequestType with id " + id);
			return new ResponseEntity<>(requestTypeService.getById(id), HttpStatus.OK);
		} catch (Exception e) {
			LOG.error("Error inesperado", e);
			throw e;
		}
	}

	@RequestMapping(value = "", method = RequestMethod.GET)
	public ResponseEntity<?> getAll() {
		try {
			LOG.info("Getting all RequestTypes");
			return new ResponseEntity<>(requestTypeService.getAll(), HttpStatus.OK);
		} catch (Exception e) {
			LOG.error("Error inesperado", e);
			throw e;
		}
	}

}
