package com.adistec.intranet.controller;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.RestController;
import org.springframework.web.multipart.MultipartFile;
import com.adistec.intranet.dto.ContactFormLogDTO;
import com.adistec.intranet.service.ContactFormLogService;
import com.fasterxml.jackson.databind.DeserializationFeature;
import com.fasterxml.jackson.databind.ObjectMapper;

@RestController
@RequestMapping(value = "/contactFormLog")
public class ContactFormLogController {

	private static final Logger LOG = LoggerFactory.getLogger(ContactFormLogController.class);

	@Autowired
	private ContactFormLogService contactFormLogService;

	@RequestMapping(value = "/{id}", method = RequestMethod.GET)
	public ResponseEntity<?> getByid(@PathVariable Integer id) {
		try {
			LOG.info("Getting ContactFormLog with id " + id);
			return new ResponseEntity<>(contactFormLogService.getById(id), HttpStatus.OK);
		} catch (Exception e) {
			LOG.error("Error inesperado", e);
			throw e;
		}
	}

	@RequestMapping(value = "", method = RequestMethod.POST)
	public ResponseEntity<?> save(@RequestParam String contactFormLog, @RequestParam MultipartFile file) {
		try {
			ObjectMapper objectMapper = new ObjectMapper();
			objectMapper.configure(DeserializationFeature.FAIL_ON_UNKNOWN_PROPERTIES, false);
			ContactFormLogDTO contactFormLogDTO = objectMapper.readValue(contactFormLog, ContactFormLogDTO.class);
			LOG.info("Saving referral with email: " + contactFormLogDTO.getEmail());
			ContactFormLogDTO savedContactFormLog = contactFormLogService.saveAndFlush(contactFormLogDTO, file);
			return ResponseEntity.ok(savedContactFormLog);
		} catch (Exception e) {
			LOG.error("Error saving referral", e);
			return new ResponseEntity<>(HttpStatus.INTERNAL_SERVER_ERROR);
		}
	}
}
