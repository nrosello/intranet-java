package com.adistec.intranet.controller;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.RestController;
import org.springframework.web.multipart.MultipartFile;
import com.adistec.intranet.dto.HelpRequestDTO;
import com.adistec.intranet.service.HelpRequestService;
import com.fasterxml.jackson.databind.DeserializationFeature;
import com.fasterxml.jackson.databind.ObjectMapper;

@RestController
@RequestMapping(value = "/helpRequest")
public class HelpRequestController {

	private static final Logger LOG = LoggerFactory.getLogger(HelpRequestController.class);

	@Autowired
	private HelpRequestService helpRequestService;

	@RequestMapping(value = "/{id}", method = RequestMethod.GET)
	public ResponseEntity<?> getByid(@PathVariable Integer id) {
		try {
			LOG.info("Getting Help request with id " + id);
			return new ResponseEntity<>(helpRequestService.getById(id), HttpStatus.OK);
		} catch (Exception e) {
			LOG.error("Error inesperado", e);
			throw e;
		}
	}

	@RequestMapping(value = "", method = RequestMethod.POST)
	public ResponseEntity<?> add(@RequestParam String helpRequest, @RequestParam MultipartFile[] files) {
		try {
			ObjectMapper objectMapper = new ObjectMapper();
			objectMapper.configure(DeserializationFeature.FAIL_ON_UNKNOWN_PROPERTIES, false);
			HelpRequestDTO helpRequestDTO = objectMapper.readValue(helpRequest, HelpRequestDTO.class);
			LOG.info("Pedido de ticket de email: " + helpRequestDTO.getEmail());
			HelpRequestDTO savedRequest = helpRequestService.saveAndFlush(helpRequestDTO, files);
			return ResponseEntity.ok(savedRequest);
		} catch (Exception e) {
			LOG.error("Error al agregar el request", e);
			return new ResponseEntity<>(HttpStatus.INTERNAL_SERVER_ERROR);
		}
	}

}
