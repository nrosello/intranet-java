package com.adistec.intranet.controller;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RestController;
import com.adistec.intranet.service.AreaService;

@RestController
@RequestMapping(value = "/area")
public class AreaController {

	private static final Logger LOG = LoggerFactory.getLogger(AreaController.class);

	@Autowired
	private AreaService areaService;

	@RequestMapping(value = "", method = RequestMethod.GET)
	public ResponseEntity<?> getAll() {
		try {
			LOG.info("Getting all areas ");
			return new ResponseEntity<>(areaService.getAll(), HttpStatus.OK);
		} catch (Exception e) {
			LOG.error("Error inesperado", e);
			throw e;
		}
	}
}
