package com.adistec.intranet.controller;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.http.HttpEntity;
import org.springframework.http.HttpHeaders;
import org.springframework.http.HttpMethod;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RestController;
import org.springframework.web.client.RestTemplate;

import com.adistec.intranet.dto.SubsidiaryDTO;
import com.adistec.intranet.entity.mysql.Subsidiary;
import com.adistec.intranet.service.SubsidiaryService;

@RestController
@RequestMapping(value = "/branchOffice")
public class BranchOfficeController {

	private static final Logger LOG = LoggerFactory.getLogger(BranchOfficeController.class);

	// @Autowired
	// SubsidiaryService branchOfficeService;

	@Autowired
	private RestTemplate restTemplate;

	@Value("${microservice.baseUrl}")
	private String microServiceBaseUrl;

	// @RequestMapping(value = "", method = RequestMethod.GET)
	// public ResponseEntity<?> getAll(@PathVariable Integer id){
	// try {
	// LOG.info("Getting all branch offices ");
	// return new ResponseEntity<>(branchOfficeService.getAll(), HttpStatus.OK);
	// }catch (Exception e) {
	// LOG.error("Error inesperado", e);
	// throw e;
	// }
	// }

	@RequestMapping(value = "")
	public ResponseEntity<?> getAllCountries() {
		HttpHeaders headers = new HttpHeaders();
		// headers.set("locale", LocaleUtil.getLocale());
		HttpEntity<?> entityReq = new HttpEntity<>(SubsidiaryDTO.class, headers);
		return restTemplate.exchange(microServiceBaseUrl + "/subsidiary", HttpMethod.GET, entityReq, String.class);
	}
}
