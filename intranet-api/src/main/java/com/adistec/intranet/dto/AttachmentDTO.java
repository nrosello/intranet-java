package com.adistec.intranet.dto;

public class AttachmentDTO {

	private Integer id;
	private String attachmentname;

	public AttachmentDTO(Integer id, String attachmentname) {
		this.id = id;
		this.attachmentname = attachmentname;
	}

	public AttachmentDTO() {

	}

	public Integer getId() {
		return id;
	}

	public void setId(Integer id) {
		this.id = id;
	}

	public String getAttachmentname() {
		return attachmentname;
	}

	public void setAttachmentname(String attachmentname) {
		this.attachmentname = attachmentname;
	}

}
