package com.adistec.intranet.dto;

import java.util.Date;
import java.util.List;

public class HelpRequestDTO {
	private Integer id;
	private Date date;
	private String ip;
	private String name;
	private String email;
	private String requesttype;
	private String message;
	private List<AttachmentDTO> attachments;
	private String deliverto;
	private String delivercc;
	private String deliverbcc;
	private Boolean delivered;
	private String subsidiary;

	public HelpRequestDTO() {

	}

	public HelpRequestDTO(Integer id, Date requestdate, String requestip, String name, String email, String requesttype,
			String requestmessage, List<AttachmentDTO> attachments, String deliverto, String delivercc,
			String deliverbcc, Boolean delivered, String subsidiary) {
		this.id = id;
		this.date = requestdate;
		this.ip = requestip;
		this.name = name;
		this.email = email;
		this.requesttype = requesttype;
		this.message = requestmessage;
		this.attachments = attachments;
		this.deliverto = deliverto;
		this.delivercc = delivercc;
		this.deliverbcc = deliverbcc;
		this.delivered = delivered;
		this.subsidiary = subsidiary;
	}

	public Integer getId() {
		return id;
	}

	public void setId(Integer id) {
		this.id = id;
	}

	public String getName() {
		return name;
	}

	public void setName(String name) {
		this.name = name;
	}

	public String getRequesttype() {
		return requesttype;
	}

	public void setRequesttype(String requesttype) {
		this.requesttype = requesttype;
	}

	public List<AttachmentDTO> getAttachments() {
		return attachments;
	}

	public void setAttachments(List<AttachmentDTO> attachments) {
		this.attachments = attachments;
	}

	public String getDeliverto() {
		return deliverto;
	}

	public void setDeliverto(String deliverto) {
		this.deliverto = deliverto;
	}

	public String getDelivercc() {
		return delivercc;
	}

	public void setDelivercc(String delivercc) {
		this.delivercc = delivercc;
	}

	public Boolean getDelivered() {
		return delivered;
	}

	public void setDelivered(Boolean delivered) {
		this.delivered = delivered;
	}

	public String getSubsidiary() {
		return subsidiary;
	}

	public void setSubsidiary(String subsidiary) {
		this.subsidiary = subsidiary;
	}

	public String getEmail() {
		return email;
	}

	public void setEmail(String email) {
		this.email = email;
	}

	public Date getDate() {
		return date;
	}

	public void setDate(Date date) {
		this.date = date;
	}

	public String getIp() {
		return ip;
	}

	public void setIp(String ip) {
		this.ip = ip;
	}

	public String getMessage() {
		return message;
	}

	public void setMessage(String message) {
		this.message = message;
	}

	public String getDeliverbcc() {
		return deliverbcc;
	}

	public void setDeliverbcc(String deliverbcc) {
		this.deliverbcc = deliverbcc;
	}

}
