package com.adistec.intranet.dto;

import java.util.List;

import com.fasterxml.jackson.annotation.JsonProperty;

import lombok.Data;
import lombok.NoArgsConstructor;

/**
 * Created by gparis on 1/3/2017.
 */
@Data
@NoArgsConstructor
public class UserDTO {
    @JsonProperty(value = "email")
    private String userName;
    @JsonProperty(value = "name")
    private String firstName;
    @JsonProperty(value = "lastname")
    private String lastName;
    @JsonProperty(value = "country")
    private String country;
    @JsonProperty(value = "locale")
    private String locale;
    @JsonProperty
    private List<String> roles;
    @JsonProperty
    private List<String> permissions;


    private UserDTO(String userName, String firstName, String lastName, String country, String locale, List<String> roles,List<String> permissions) {
        this.userName = userName;
        this.firstName = firstName;
        this.lastName = lastName;
        this.country = country;
        this.locale = locale;
        this.roles = roles;
        this.permissions = permissions;
    }

    public static class Builder {
        private String userName;
        private String firstName;
        private String lastName;
        private String country;
        private String locale;
        private List<String> roles;
        private List<String> permissions;

        public Builder userName(final String userName) {
            this.userName = userName;
            return this;
        }

        public Builder firstName(final String firstName) {
            this.firstName = firstName;
            return this;
        }

        public Builder lastName(final String lastName) {
            this.lastName = lastName;
            return this;
        }

        public Builder country(final String country) {
            this.country = country;
            return this;
        }

        public Builder locale(final String locale) {
            this.locale = locale;
            return this;
        }
        
        public Builder roles(final List<String> roles) {
            this.roles = roles;
            return this;
        }

        public Builder permissions(final List<String> permissions) {
            this.permissions = permissions;
            return this;
        }

        public UserDTO build() {
            return new UserDTO(userName, firstName, lastName, country, locale, roles,permissions);
        }
    }

	public List<String> getRoles() {
		return roles;
	}

	public void setRoles(List<String> roles) {
		this.roles = roles;
	}
}
