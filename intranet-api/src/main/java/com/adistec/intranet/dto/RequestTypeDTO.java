package com.adistec.intranet.dto;

public class RequestTypeDTO {

	private Integer id;
	private String subject;
	private String deliverto;
	private String delivercc;
	private String deliverbcc;

	public RequestTypeDTO() {

	}

	public RequestTypeDTO(Integer id, String subject, String deliverto, String delivercc, String deliverbcc) {
		this.id = id;
		this.subject = subject;
		this.deliverto = deliverto;
		this.delivercc = delivercc;
		this.deliverbcc = deliverbcc;
	}

	public Integer getId() {
		return id;
	}

	public void setId(Integer id) {
		this.id = id;
	}

	public String getSubject() {
		return subject;
	}

	public void setSubject(String subject) {
		this.subject = subject;
	}

	public String getDeliverto() {
		return deliverto;
	}

	public void setDeliverto(String deliverto) {
		this.deliverto = deliverto;
	}

	public String getDelivercc() {
		return delivercc;
	}

	public void setDelivercc(String delivercc) {
		this.delivercc = delivercc;
	}

	public String getDeliverbcc() {
		return deliverbcc;
	}

	public void setDeliverbcc(String deliverbcc) {
		this.deliverbcc = deliverbcc;
	}

}
