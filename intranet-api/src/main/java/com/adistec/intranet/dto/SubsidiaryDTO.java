package com.adistec.intranet.dto;

public class SubsidiaryDTO {
	private long id;
	private String country;

	public long getId() {
		return id;
	}

	public String getCountry() {
		return country;
	}

	public void setId(long id) {
		this.id = id;
	}

	public void setCountry(String country) {
		this.country = country;
	}

}
