package com.adistec.intranet.util;

import java.io.File;
import java.io.FileOutputStream;
import java.io.IOException;

import org.springframework.web.multipart.MultipartFile;

public class FileUtils {
	/**
	 * 
	 * @param path Must end with /
	 * @param file
	 * @return Filename of uploaded file
	 * @throws IOException 
	 */
	public static String uploadFile(String path, MultipartFile file) throws IOException {
		String fileName = System.currentTimeMillis() + file.getOriginalFilename();
		File uploadedFile = new File(path + fileName);
		uploadedFile.createNewFile();
		FileOutputStream fos = new FileOutputStream(uploadedFile);
		fos.write(file.getBytes());
		fos.close();
		return fileName;
	}
}
