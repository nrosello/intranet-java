package com.adistec.intranet.interceptors;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import org.slf4j.MDC;
import org.springframework.security.core.context.SecurityContextHolder;
import org.springframework.web.servlet.handler.HandlerInterceptorAdapter;

/**
 * Agrega el nombre de usuario al contexto para que al momento de hacer logging
 * aparezca siempre
 */
public class LoggingCommonsInterceptor extends HandlerInterceptorAdapter {

	@Override
	public boolean preHandle(HttpServletRequest request, HttpServletResponse response, Object handler)
			throws Exception {
		try {
			MDC.put("username", SecurityContextHolder.getContext().getAuthentication().getPrincipal().toString());
		} catch (NullPointerException e) {
			/** no logueado aún **/
		}
		return true;
	}
}
