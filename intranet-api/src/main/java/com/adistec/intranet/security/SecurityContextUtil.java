package com.adistec.intranet.security;

import org.springframework.security.core.context.SecurityContextHolder;
import org.springframework.security.oauth2.provider.OAuth2Authentication;

import java.util.List;
import java.util.Map;

public class SecurityContextUtil {
	public static final String FIRST_NAME = "firstName";
	public static final String LAST_NAME = "lastName";
	public static final String COUNTRY = "country";
	public static final String USER_NAME = "username";
	public static final String LOCALE = "locale";
	public static final String ENCRYPTED_PASSWORD = "password";
	public static final String COMPANY_ID = "companyId";
	public static final String PERMISSIONS = "permissions";

	private static final String CUSTOM_PROPERTIES = "customProperties";
	private static final String USER_AUTHENTICATOR = "userAuthentication";
	private static final String PRINCIPAL = "principal";

	@SuppressWarnings("unchecked")
	public static String getCustomProperty(String propertyName) {
		OAuth2Authentication authentication = (OAuth2Authentication) SecurityContextHolder.getContext()
				.getAuthentication();
		Map<String, Object> details = (Map<String, Object>) authentication.getUserAuthentication().getDetails();
		return ((Map<String, Map<String, Map<String, String>>>) details.get(USER_AUTHENTICATOR)).get(PRINCIPAL)
				.get(CUSTOM_PROPERTIES).get(propertyName);
	}

	public static List<String> getCustomList(String propertyName) {
		OAuth2Authentication authentication = (OAuth2Authentication) SecurityContextHolder.getContext()
				.getAuthentication();
		Map<String, Object> details = (Map<String, Object>) authentication.getUserAuthentication().getDetails();
		return ((Map<String, Map<String, List<String>>>) details.get(USER_AUTHENTICATOR)).get(PRINCIPAL)
				.get(propertyName);
	}
}
