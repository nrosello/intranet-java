package com.adistec.intranet.mapper;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;

import com.adistec.intranet.dto.ContactFormLogDTO;
import com.adistec.intranet.entity.common.IContactFormLog;
import com.adistec.intranet.entity.mysql.ContactFormLogMysql;
import com.adistec.intranet.entity.sqlserver.ContactFormLogSqlServer;
import com.adistec.intranet.repository.mysql.ContactFormLogMysqlRepository;
import com.adistec.intranet.repository.sqlserver.ContactFormLogSqlServerRepository;

@Component
public class ContactFormLogMapper {

	@Autowired
	private ContactFormLogMysqlRepository contactFormLogMysqlRepository;

	@Autowired
	private ContactFormLogSqlServerRepository contactFormLogSqlServerRepository;

	public ContactFormLogMysql toContactFormLogMysql(ContactFormLogDTO contactFormLogDTO) {
		ContactFormLogMysql contactFormLog;
		if (contactFormLogDTO.getId() != null) {
			contactFormLog = contactFormLogMysqlRepository.findById(contactFormLogDTO.getId());
		} else {
			contactFormLog = new ContactFormLogMysql();
		}
		setContactFormLogData(contactFormLogDTO, contactFormLog);
		return contactFormLog;
	}

	public ContactFormLogSqlServer toContactFormLogSqlServer(ContactFormLogDTO contactFormLogDTO) {
		ContactFormLogSqlServer contactFormLog;
		if (contactFormLogDTO.getId() != null) {
			contactFormLog = contactFormLogSqlServerRepository.findById(contactFormLogDTO.getId());
		} else {
			contactFormLog = new ContactFormLogSqlServer();
		}
		setContactFormLogData(contactFormLogDTO, contactFormLog);
		return contactFormLog;
	}

	public void setContactFormLogData(ContactFormLogDTO contactFormLogDTO, IContactFormLog contactFormLog) {
		contactFormLog.setCountry(contactFormLogDTO.getCountry());
		contactFormLog.setCountrySource(contactFormLogDTO.getCountrySource());
		contactFormLog.setDateCreated(contactFormLogDTO.getDateCreated());
		contactFormLog.setEmail(contactFormLogDTO.getEmail());
		contactFormLog.setEmailReference(contactFormLogDTO.getEmailReference());
		contactFormLog.setFirstName(contactFormLogDTO.getFirstName());
		contactFormLog.setLastName(contactFormLogDTO.getLastName());
		contactFormLog.setPhone(contactFormLogDTO.getPhone());
		contactFormLog.setPosition(contactFormLogDTO.getPosition());
		contactFormLog.setSearchComments(contactFormLogDTO.getSearchComments());
		contactFormLog.setSource(contactFormLogDTO.getSource());
		contactFormLog.setTypeReferred(contactFormLogDTO.getTypeReferred());
		contactFormLog.setUploadedFile(contactFormLogDTO.getUploadedFile());
	}

	public ContactFormLogDTO toContactFormLogDTO(IContactFormLog contactFormLog) {
		return new ContactFormLogDTO(contactFormLog.getId(), contactFormLog.getFirstName(),
				contactFormLog.getLastName(), contactFormLog.getEmail(), contactFormLog.getPhone(),
				contactFormLog.getCountry(), contactFormLog.getDateCreated(), contactFormLog.getSource(),
				contactFormLog.getPosition(), contactFormLog.getCountrySource(), contactFormLog.getUploadedFile(),
				contactFormLog.getEmailReference(), contactFormLog.getTypeReferred(),
				contactFormLog.getSearchComments());
	}

}
