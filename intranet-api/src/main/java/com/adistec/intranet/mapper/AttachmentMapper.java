package com.adistec.intranet.mapper;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;

import com.adistec.intranet.dto.AttachmentDTO;
import com.adistec.intranet.entity.mysql.Attachment;
import com.adistec.intranet.repository.mysql.AttachmentRepository;

@Component
public class AttachmentMapper {

	@Autowired
	private AttachmentRepository attachmentRepository;
	
	public Attachment toAttachment(AttachmentDTO attachmentDTO) {
		Attachment attachment;
		if (attachmentDTO.getId() != null) {
			attachment = attachmentRepository.findById(attachmentDTO.getId());
		} else {
			attachment = new Attachment();
		}
		attachment.setId(attachmentDTO.getId());
		attachment.setAttachmentname(attachmentDTO.getAttachmentname());
		return attachment;
	}

	public AttachmentDTO toAttachmentDTO(Attachment attachment) {
		return new AttachmentDTO(attachment.getId(), attachment.getAttachmentname());
	}
}
