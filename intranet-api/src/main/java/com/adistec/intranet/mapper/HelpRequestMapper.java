package com.adistec.intranet.mapper;

import java.util.ArrayList;
import java.util.List;
import java.util.stream.Collectors;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;

import com.adistec.intranet.dto.AttachmentDTO;
import com.adistec.intranet.dto.HelpRequestDTO;
import com.adistec.intranet.entity.mysql.Attachment;
import com.adistec.intranet.entity.mysql.HelpRequest;
import com.adistec.intranet.repository.mysql.HelpRequestRepository;

@Component
public class HelpRequestMapper {

	@Autowired
	private HelpRequestRepository helpRequestRepository;

	@Autowired
	private AttachmentMapper attachmentMapper;

	public HelpRequest toHelpRequest(HelpRequestDTO helpRequestDTO) {
		HelpRequest helpRequest;
		if (helpRequestDTO.getId() != null) {
			helpRequest = helpRequestRepository.findById(helpRequestDTO.getId());
		} else {
			helpRequest = new HelpRequest();
		}
		helpRequest.setDate(helpRequestDTO.getDate());
		helpRequest.setIp(helpRequestDTO.getIp());
		helpRequest.setName(helpRequestDTO.getName());
		helpRequest.setEmail(helpRequestDTO.getEmail());
		helpRequest.setRequesttype(helpRequestDTO.getRequesttype());
		helpRequest.setMessage(helpRequestDTO.getMessage());
		if (helpRequestDTO.getAttachments() != null) {
			if (helpRequest.getAttachments() != null) {
				helpRequest.getAttachments().clear();
			} else {
				helpRequest.setAttachments(new ArrayList<Attachment>());
			}

			helpRequest.getAttachments().addAll(helpRequestDTO.getAttachments().stream()
					.map(attachmentDTO -> attachmentMapper.toAttachment(attachmentDTO)).collect(Collectors.toList()));
		}
		helpRequest.setDeliverto(helpRequestDTO.getDeliverto());
		helpRequest.setDelivercc(helpRequestDTO.getDelivercc());
		helpRequest.setDeliverbcc(helpRequestDTO.getDeliverbcc());
		helpRequest.setDelivered(helpRequestDTO.getDelivered());
		helpRequest.setSubsidiary(helpRequestDTO.getSubsidiary());
		return helpRequest;
	}

	public HelpRequestDTO toHelpRequestDTO(HelpRequest helpRequest) {
		List<AttachmentDTO> attachments = new ArrayList<AttachmentDTO>();
		if (helpRequest.getAttachments() != null)
			attachments = helpRequest.getAttachments().stream()
					.map(attachment -> new AttachmentDTO(attachment.getId(), attachment.getAttachmentname()))
					.collect(Collectors.toList());
		return new HelpRequestDTO(helpRequest.getId(), helpRequest.getDate(), helpRequest.getIp(),
				helpRequest.getName(), helpRequest.getEmail(), helpRequest.getRequesttype(), helpRequest.getMessage(),
				attachments, helpRequest.getDeliverto(), helpRequest.getDelivercc(), helpRequest.getDeliverbcc(),
				helpRequest.getDelivered(), helpRequest.getSubsidiary());
	}
}
