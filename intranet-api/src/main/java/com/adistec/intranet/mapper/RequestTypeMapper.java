package com.adistec.intranet.mapper;

import org.springframework.stereotype.Component;

import com.adistec.intranet.dto.RequestTypeDTO;
import com.adistec.intranet.entity.mysql.RequestType;

@Component
public class RequestTypeMapper {

	public RequestType toRequestType(RequestTypeDTO requestTypeDTO) {
		RequestType requestType = new RequestType();
		requestType.setId(requestTypeDTO.getId());
		requestType.setSubject(requestTypeDTO.getSubject());
		requestType.setDeliverto(requestTypeDTO.getDeliverto());
		requestType.setDelivercc(requestTypeDTO.getDelivercc());
		requestType.setDeliverbcc(requestTypeDTO.getDeliverbcc());
		return requestType;
	}

	public RequestTypeDTO toRequestTypeDTO(RequestType requestType) {
		return new RequestTypeDTO(requestType.getId(), requestType.getSubject(), requestType.getDeliverto(),
				requestType.getDelivercc(), requestType.getDeliverbcc());
	}
}
