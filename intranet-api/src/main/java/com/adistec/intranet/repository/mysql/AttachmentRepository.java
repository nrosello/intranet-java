package com.adistec.intranet.repository.mysql;

import org.springframework.data.jpa.repository.JpaRepository;

import com.adistec.intranet.entity.mysql.Attachment;

public interface AttachmentRepository extends JpaRepository<Attachment, Long>{
	Attachment findById(Integer id);
}
