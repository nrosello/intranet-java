package com.adistec.intranet.repository.mysql;

import java.util.List;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;
import org.springframework.stereotype.Repository;

import com.adistec.intranet.entity.mysql.Area;

@Repository
public interface AreaRepository extends JpaRepository<Area, Long> {

	@Query("SELECT a FROM Area a WHERE id != 'eje' AND id != 'Internal Training'")
	List<Area> findAll();

}
