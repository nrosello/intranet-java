package com.adistec.intranet.repository.mysql;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;

import com.adistec.intranet.entity.mysql.HelpRequest;

@Repository
public interface HelpRequestRepository extends JpaRepository<HelpRequest, Long> {
	HelpRequest findById(Integer id);
}
