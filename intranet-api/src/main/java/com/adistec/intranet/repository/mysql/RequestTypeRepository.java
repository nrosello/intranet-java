package com.adistec.intranet.repository.mysql;

import java.util.List;

import org.springframework.data.jpa.repository.JpaRepository;

import com.adistec.intranet.entity.mysql.RequestType;

public interface RequestTypeRepository extends JpaRepository<RequestType, Long> {

	public RequestType findById(Integer id);

	public List<RequestType> findAll();
}
