package com.adistec.intranet.repository.mysql;

import org.springframework.data.jpa.repository.JpaRepository;

import com.adistec.intranet.entity.mysql.ContactFormLogMysql;

public interface ContactFormLogMysqlRepository extends JpaRepository<ContactFormLogMysql, Long> {

	ContactFormLogMysql findById(Integer id);
}
