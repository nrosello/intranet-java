package com.adistec.intranet.repository.sqlserver;

import org.springframework.data.jpa.repository.JpaRepository;
import com.adistec.intranet.entity.sqlserver.ContactFormLogSqlServer;

public interface ContactFormLogSqlServerRepository extends JpaRepository<ContactFormLogSqlServer, Long> {

	ContactFormLogSqlServer findById(Integer id);
}
