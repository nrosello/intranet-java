package com.adistec.intranet.configuration;

import com.adistec.mailclient.impl.MailClient;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;

@Configuration
public class MailConfiguration {

    @Value("${spring.services.endpoint.mail}")
    private String mailSenderUrl;

    @Bean
    public MailClient mailClient() {
        MailClient client = new MailClient(mailSenderUrl);
        return client;
    }
}