package com.adistec.intranet.configuration;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;

import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;

import springfox.documentation.builders.AuthorizationCodeGrantBuilder;
import springfox.documentation.builders.OAuthBuilder;
import springfox.documentation.builders.PathSelectors;
import springfox.documentation.builders.RequestHandlerSelectors;
import springfox.documentation.service.ApiKey;
import springfox.documentation.service.AuthorizationScope;
import springfox.documentation.service.GrantType;
import springfox.documentation.service.OAuth;
import springfox.documentation.service.ResourceOwnerPasswordCredentialsGrant;
import springfox.documentation.service.SecurityReference;
import springfox.documentation.service.SecurityScheme;
import springfox.documentation.service.TokenEndpoint;
import springfox.documentation.service.TokenRequestEndpoint;
import springfox.documentation.spi.DocumentationType;
import springfox.documentation.spi.service.contexts.SecurityContext;
import springfox.documentation.spring.web.plugins.Docket;
import springfox.documentation.swagger.web.SecurityConfiguration;
import springfox.documentation.swagger.web.SecurityConfigurationBuilder;
import springfox.documentation.swagger2.annotations.EnableSwagger2;

@Configuration
@EnableSwagger2
public class SwaggerConfig {
	private static final String AUTH_SERVER = "http://192.168.51.125/auth/oauth";
	private static final String CLIENT_ID = "SampleClientId";
	private static final String CLIENT_SECRET = "secret";

	@Bean
	public Docket api() {
		return new Docket(DocumentationType.SWAGGER_2).select().apis(RequestHandlerSelectors.any())
				.paths(PathSelectors.any()).build();
//				.securitySchemes(Arrays.asList(securityScheme()))
//				.securityContexts(Arrays.asList(securityContext()));
	}

//	@Bean
//	public SecurityConfiguration security() {
//		return SecurityConfigurationBuilder.builder().clientId("SampleClientId").clientSecret("secret")
//				.scopeSeparator(" ").useBasicAuthenticationWithAccessCodeGrant(false).build();
//	}
//
//	private OAuth securityScheme() {
//		GrantType grantType = new AuthorizationCodeGrantBuilder()
//				.tokenEndpoint(new TokenEndpoint("http://192.168.51.125/auth/oauth" + "/token", "oauthtoken"))
//				.tokenRequestEndpoint(new TokenRequestEndpoint("http://192.168.51.125/auth/oauth" + "/authorize",
//						"SampleClientId", "secret"))
//				.build();
//
//		OAuth oauth = new OAuthBuilder().name("spring_oauth").grantTypes(Arrays.asList(grantType))
//				.scopes(Arrays.asList(scopes())).build();
//		return oauth;
//	}
//
//	private AuthorizationScope[] scopes() {
//		AuthorizationScope[] scopes = { new AuthorizationScope("user_info", "for read operations") };
//		return scopes;
//	}
//
//	private SecurityContext securityContext() {
//		return SecurityContext.builder()
//				.securityReferences(Arrays.asList(new SecurityReference("spring_oauth", scopes())))
//				.forPaths(PathSelectors.any()).build();
//	}
}
