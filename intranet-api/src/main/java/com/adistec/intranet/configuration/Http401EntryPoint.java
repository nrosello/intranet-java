package com.adistec.intranet.configuration;

import java.io.IOException;

import javax.servlet.ServletException;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import org.springframework.beans.factory.InitializingBean;
import org.springframework.security.core.AuthenticationException;
import org.springframework.security.web.AuthenticationEntryPoint;
import org.springframework.security.web.PortResolver;
import org.springframework.security.web.PortResolverImpl;
import org.springframework.security.web.savedrequest.DefaultSavedRequest;

public class Http401EntryPoint implements AuthenticationEntryPoint, InitializingBean {
	private static final String LOGIN_LOCATION = "X-Login-at";
	private static final String SAVED_REQUEST = "SPRING_SECURITY_SAVED_REQUEST";

	public void commence(HttpServletRequest request, HttpServletResponse response,
			AuthenticationException authException) throws IOException, ServletException {
		response.setStatus(HttpServletResponse.SC_UNAUTHORIZED);
		response.setHeader(LOGIN_LOCATION, "/login");
		DefaultSavedRequest savedRequest = new RefererRedirectSavedRequest(request, new PortResolverImpl());
		request.getSession().setAttribute(SAVED_REQUEST, savedRequest);
	}

	@Override
	public void afterPropertiesSet() throws Exception {
	}

	class RefererRedirectSavedRequest extends DefaultSavedRequest {
		private static final long serialVersionUID = -2888055354638723357L;
		private String redirectUrl;

		public RefererRedirectSavedRequest(HttpServletRequest request, PortResolver portResolver) {
			super(request, portResolver);
			this.redirectUrl = request.getHeader("Referer");
		}

		@Override
		public String getRedirectUrl() {
			return redirectUrl;
		}

	}
}
