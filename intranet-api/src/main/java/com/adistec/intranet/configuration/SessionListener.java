//package com.adistec.intranet.configuration;
//
//import org.springframework.context.ApplicationListener;
//import org.springframework.security.web.session.HttpSessionCreatedEvent;
//
//public class SessionListener implements ApplicationListener<HttpSessionCreatedEvent> {
//	private static final int TWELVE_HOURS = 43200;
//
//	@Override
//	public void onApplicationEvent(HttpSessionCreatedEvent event) {
//		event.getSession().setMaxInactiveInterval(TWELVE_HOURS);
//	}
//}
