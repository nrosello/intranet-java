package com.adistec.intranet.configuration;

import java.util.HashMap;

import javax.naming.NamingException;
import javax.sql.DataSource;

import org.springframework.beans.factory.annotation.Value;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;
import org.springframework.context.annotation.PropertySource;
import org.springframework.data.jpa.repository.config.EnableJpaRepositories;
import org.springframework.jndi.JndiTemplate;
import org.springframework.orm.jpa.JpaTransactionManager;
import org.springframework.orm.jpa.LocalContainerEntityManagerFactoryBean;
import org.springframework.orm.jpa.vendor.HibernateJpaVendorAdapter;
import org.springframework.transaction.PlatformTransactionManager;

@Configuration
@PropertySource({ "classpath:databases.properties" })
@EnableJpaRepositories(basePackages = "com.adistec.intranet.repository.sqlserver", entityManagerFactoryRef = "sqlserverEntityManager", transactionManagerRef = "sqlserverTransactionManager")
public class SqlServerConfiguration {

	@Value("${sqlserver.datasource}")
	private String sqlServerJndi;

	@Bean
	public LocalContainerEntityManagerFactoryBean sqlserverEntityManager() throws NamingException {
		LocalContainerEntityManagerFactoryBean em = new LocalContainerEntityManagerFactoryBean();
		em.setDataSource(sqlserverDataSource());
		em.setPackagesToScan(new String[] { "com.adistec.intranet.entity.sqlserver" });
		HibernateJpaVendorAdapter vendorAdapter = new HibernateJpaVendorAdapter();
		em.setJpaVendorAdapter(vendorAdapter);
		HashMap<String, Object> properties = new HashMap<>();
		em.setJpaPropertyMap(properties);
		return em;
	}

	@Bean
	public PlatformTransactionManager sqlserverTransactionManager() throws NamingException {
		JpaTransactionManager transactionManager = new JpaTransactionManager();
		transactionManager.setEntityManagerFactory(sqlserverEntityManager().getObject());
		return transactionManager;
	}

	@Bean
	public DataSource sqlserverDataSource() throws NamingException {
		JndiTemplate jndiTemplate = new JndiTemplate();
		return (DataSource) jndiTemplate.lookup(sqlServerJndi);
	}
}
