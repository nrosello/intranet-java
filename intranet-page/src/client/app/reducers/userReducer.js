/**
 * Created by gparis on 8/31/2017.
 */
import { USER_INFO, TOGGLE_MENU } from '../actions/types';

const INITIAL_STATE = {name: '', lastName: '', email: '', open: false, locale: 'en', auth: false, roles:[],permissions:[]};

export default (state = INITIAL_STATE, action) => {
    switch(action.type) {
        case USER_INFO:
            return {...state, name: action.payload.name, lastName: action.payload.lastname,
            	email: action.payload.email, auth: true, roles: action.payload.roles,permissions: action.payload.permissions, locale: action.payload.locale};
        case TOGGLE_MENU:
            return {...state, open: !state.open};
    }

    return state;
}
