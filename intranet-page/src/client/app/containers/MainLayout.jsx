import React from 'react';
import { connect } from 'react-redux';
import { Header , Loader , Footer } from 'adistec-components/dist/adistec-react-component.js';
import { getUserInfo, toggleMenu } from '../actions/userActions';
import {FormattedMessage} from 'react-intl';

//const loaderStyle = {margin: 'auto',display:'block',height: '100px',width: '100px', position:'fixed'};
const items =
    [{label: <FormattedMessage id='menuActions.requests'/>, path: ''},
    {label: <FormattedMessage id='menuActions.newRequest'/>, path: 'request'}/*,
    {label: 'Forms (RRHH)', path: 'formsRRHH'},
    {label: 'Forms (Marketing)', path: 'formsMarketing'}*/];

class MainLayout extends React.Component {
    constructor(props) {
        super(props);
        this.state = {open: false};
    }

    componentDidMount() {
        this.props.getUserInfo();
    }

    toggleDrawer = () => {
        this.setState({open: !this.state.open});
    };

    handleTouchTap = (e) => {
        if(e !== 'clickAway')
            e.preventDefault();
        return this.props.toggleMenu();
    };

    render() {
        return (
            this.props.user.auth ?
                <div className="appWrapper">
                    <Header toggleDrawer={this.toggleDrawer}
                            drawerOpen={this.state.open}
                            drawerItems={items}
                            menuTitle={<FormattedMessage id='menuActions.appTitle'/>}
                            open={this.props.user.open}
                            onClick={this.handleTouchTap}
                            username={`${this.props.user.name} ${this.props.user.lastName}`}
                            userEmail={this.props.user.email}/>
                    <div id="main-wrapper">
                    {this.props.counter.ready != 0 ? <Loader /> : null}
                        <main style={this.props.counter.ready != 0 ? {opacity:0.2} : null}>
                            {this.props.children}
                        </main>
                    </div>
                    <Footer/>
                </div> : null
        );
    }
}


export default connect(
    state => {
        return {
            user: state.user,
            counter: state.loader.counter
        }
    }, { getUserInfo, toggleMenu}
)(MainLayout);
