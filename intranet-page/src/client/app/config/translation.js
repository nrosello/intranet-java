const mergeTranslations = (requireContext) => {
  const merged = {};
  requireContext.keys().forEach((key) => {
    Object.assign(merged, requireContext(key));
  });
  return merged;
};

/* eslint-disable */
const translations = {
  es: mergeTranslations(require.context('./i18n/es', false, /.json$/)),
  en: mergeTranslations(require.context('./i18n/en', false, /.json$/)),
  pt: mergeTranslations(require.context('./i18n/pt', false, /.json$/))
};
/* eslint-enable */

//let currentLocale;
//const savedLocale = localStorage.getItem('locale') || 'es';

export const locales = Object.keys(translations);

export const translatedMessages = (locale) => {
  return flattenMessages(translations[locale]);
}
const flattenMessages = ((nestedMessages, prefix = '') => {
  if (nestedMessages === null) {
    return {}
  }
  return Object.keys(nestedMessages).reduce((messages, key) => {
    const value       = nestedMessages[key]
    const prefixedKey = prefix ? `${prefix}.${key}` : key

    if (typeof value === 'string') {
      Object.assign(messages, { [prefixedKey]: value })
    } else {
      Object.assign(messages, flattenMessages(value, prefixedKey))
    }

    return messages
  }, {})
});
