import axiosClass from 'axios';
import { $ } from 'zepto-browserify';

const getCookie = (name) => {
    let cookieValue = null;
    if (document.cookie && document.cookie !== '') {
        let cookies = document.cookie.split(';');
        for (let i = 0; i < cookies.length; i++) {
            let cookie = $.trim(cookies[i]);
            if (cookie.substring(0, name.length + 1) === (name + '=')) {
                cookieValue = decodeURIComponent(cookie.substring(name.length + 1));
                break;
            }
        }
    }
    return cookieValue;
};

const instance = axiosClass.create({ baseURL: _API });

instance.interceptors.request.use(config => {
    config.headers = {...config.headers, 'X-CSRF-TOKEN': getCookie('X-CSRF-TOKEN')};
    return config;
}, undefined);

instance.interceptors.response.use(undefined, error => {
    if(error.response.status === 401) {
        window.location.href = _API + (error.response.headers['x-login-at'] || '/login');
    }
    else {
        return Promise.reject(error);
    }
});

export let axios = instance;