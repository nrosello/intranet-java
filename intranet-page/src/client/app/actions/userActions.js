/**
 * Created by gparis on 8/31/2017.
 */
import { axios } from './axiosWrapper';
import { USER_INFO, TOGGLE_MENU } from '../actions/types';


const user = {
	name: 'Change Me', lastName: 'Change Me', email: 'ChangeMe@email.com', open: false, locale: 'en', auth: true, roles:[],permissions:[]
}

export const getUserInfo = () => {
    return dispatch => {
        dispatch({type: USER_INFO, payload: user})
        /*axios.get('/user/me').then(response => {
            dispatch({type: USER_INFO, payload: response.data})
        })*/
    }
};

export const toggleMenu = () => {
    return dispatch => {
        dispatch({type: TOGGLE_MENU});
    }
};