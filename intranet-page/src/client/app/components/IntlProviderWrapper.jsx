/*import React from 'react';
import IntlProvider from 'react-intl';
import { connect } from 'react-redux';
import { translatedMessages } from '../config/translation';

export default class IntlProviderWrapper extends React.Component {
    constructor(props) {
        super(props);
    }

    render(){
		return (
      <IntlProvider locale={'es'} messages={''}>
        {this.props.children}
      </IntlProvider>
    );}
}*/
import React from 'react';
import { connect } from 'react-redux';
import { IntlProvider } from 'react-intl';
import { translatedMessages } from '../config/translation';

class IntlProviderWrapper extends React.Component {
  constructor(props) {
      super(props);
      this.state = {
         locale:'en'
     }
  }

  changeLocaleState = (locale) => {
    if(this.state.locale != locale){
      this.setState({locale:locale});
    }
  } 


  render(){
    return (
      <IntlProvider locale={this.state.locale} messages={translatedMessages(this.state.locale)} children={this.props.children} />
    );
  }
}


export default IntlProviderWrapper;
