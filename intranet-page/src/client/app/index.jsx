import React from "react";
import ReactDOM from "react-dom";
import {Router, browserHistory } from "react-router";
import injectTapEventPlugin from 'react-tap-event-plugin';
import MuiThemeProvider from 'material-ui/styles/MuiThemeProvider';
import { Provider } from 'react-redux';
import reduxThunk from 'redux-thunk';
import { createStore, applyMiddleware, compose } from 'redux';
import getMuiTheme from 'material-ui/styles/getMuiTheme';
import routes from './constants/routes.jsx';
import reducers from './reducers/index';
import { cacheEnhancer } from 'redux-cache'
import {IntlProvider, addLocaleData} from 'react-intl';
import es from 'react-intl/locale-data/es';
import pt from 'react-intl/locale-data/pt';
import DevTools from './config/devtools';
import { registerLocales } from './config/translation';
import './assets/main.scss';
import IntlProviderWrapper from './components/IntlProviderWrapper.jsx';



injectTapEventPlugin({
    shouldRejectClick: () => document.body.querySelector('.Select-menu-outer')
});

const muiTheme = getMuiTheme({
    palette: {
        primary1Color: '#005fab',
        primary2Color: '#008fcb',
        accent1Color: '#005fab'
    },
    datePicker: {
        selectColor: '#005fab',
        color: '#005fab'
    },
    flatButton: {
        primaryTextColor: '#005fab'
    }
});

const devTools = _ENV === 'dev' ? <DevTools /> : null;

const createStoreWithMiddleware = compose(
    applyMiddleware(reduxThunk),cacheEnhancer()
   )(createStore);

const store = _ENV === 'dev' ? createStoreWithMiddleware(reducers,DevTools.instrument()) : createStoreWithMiddleware(reducers);
addLocaleData([...es, ...pt]);
    let intlProviderWrapper;

    ReactDOM.render(
      <Provider store={store}>
        <IntlProviderWrapper ref={(component) => intlProviderWrapper = component} locale={'en'}>
          <MuiThemeProvider muiTheme={muiTheme}>
                <div>
                    {devTools}
                    <Router history={browserHistory} routes={routes} />
                </div>
          </MuiThemeProvider>
        </IntlProviderWrapper>
      </Provider>
    , document.getElementById('app'));

    store.subscribe(
      () => {
        intlProviderWrapper.changeLocaleState(store.getState().user.locale);
      }
    )
