const path = require('path');
const webpack = require('webpack');
const webpackMerge = require('webpack-merge');
const commonConfig = require('./webpack-common.config.js');
const SERVER_DIR = path.resolve(__dirname, '../src/server/static');

module.exports = webpackMerge(commonConfig, {
  output: {
    path: SERVER_DIR,
    filename: '[name].js',
    publicPath: '/'
  },
  plugins: [
    new webpack.DefinePlugin({
       _API: JSON.stringify('/intranet-api'),
       _ENV: JSON.stringify('prod')
    })
  ]
});