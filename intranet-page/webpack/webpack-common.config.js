let webpack = require('webpack');
let path = require('path');

let BUILD_DIR = path.resolve(__dirname, '../src/server/static/js');
let APP_DIR = path.resolve(__dirname, '../src/client/app');
let SERVER_DIR = path.resolve(__dirname, '../src/server/static');

//plugin
const CopyWebpackPlugin = require('copy-webpack-plugin');
const HtmlWebpackPlugin = require('html-webpack-plugin');
const ExtractTextPlugin = require('extract-text-webpack-plugin');
const extractCSS = new ExtractTextPlugin('assets/styles-css.css');
const extractSASS = new ExtractTextPlugin('assets/styles-sass.css');

module.exports = {
    entry: APP_DIR + '/index.jsx',
    resolve: {
        extensions: ['.js','.jsx']
    },
    module: {
        rules: [
            {
                test: /\.jsx?$/,
                include: APP_DIR,
                loader: 'babel-loader',
                query: {
                    presets: ["es2015", "react", "stage-0"]
                }
            },
            {
                test: /\.css$/,
                use: extractCSS.extract({
                    use: 'css-loader',
                    fallback: 'style-loader',
                })
            }, 
            {
                test: /\.scss$/,
                use: extractSASS.extract({
                    fallback: 'style-loader',
                    use:['css-loader', 'postcss-loader', 'sass-loader']
                })
            },
            {
              test: /\.(jpe?g|png|gif|svg)$/i,
              loaders: [
                  {
                    loader:'file-loader?hash=sha512&digest=hex&name=[hash].[ext]',
                    options:{
                        outputPath:'/assets/img/'
                    }
                },
                  {
                  loader: 'image-webpack-loader',
                  query: {
                    gifsicle: {
                      interlaced: false
                    },
                    optipng: {
                      optimizationLevel: 7
                    }
                  }
                }
              ]
            },
           {
              test: /\.woff(2)?(\?v=[0-9]\.[0-9]\.[0-9])?$/,
              loader: 'url-loader',
              query: {
                limit: 10000,
                mimetype: "application/font-woff",
                name: 'assets/fonts/[name].[hash:7].[ext]'
              }
            },
            {
              test: /\.(eot|ttf|otf)(\?.*)?$/,
              loader: 'url-loader',
              query: {
                limit: 10000,
                name: 'assets/fonts/[name].[hash:7].[ext]'
              }
            },
        ]
    },
    plugins: [
      new CopyWebpackPlugin([
        {
          from: APP_DIR + '/assets/img/',
          to: SERVER_DIR + '/assets/img/'
        }
      ]),
        new HtmlWebpackPlugin({
            // Required
            inject: false,
            template: require('html-webpack-template'),
            // Optional
            appMountId: 'app',
            meta: [
            {
                charset: 'utf-8',
            },
            {
                name:'viewport',
                content:'width=device-width, initial-scale=1.0'
            },
            {
                name:'description',
                content:'Intranet'
            },
            {
                name:'author',
                content:'Adistec'
            },
            {
                name:'msapplication-TileColor',
                content:'#FFFFFF'
            },
            {
                name:'msapplication-TileImage',
                content:'/assets/img/favicon/mstile-144x144.png'
            },
            ],
            lang: 'en',
            links: [
                'https://fonts.googleapis.com/css?family=Roboto',
                {
                    href: '/assets/img/favicon/apple-icon-57x57.png',
                    rel: 'icon',
                    sizes: '57x57'
                },
                {
                    href: '/assets/img/favicon/apple-icon-72x72.png',
                    rel: 'icon',
                    sizes: '72x72'
                },
                {
                    href: '/assets/img/favicon/apple-icon-114x114.png',
                    rel: 'apple-touch-icon-precomposed',
                    sizes: '114x114'
                },
                {
                    href: '/assets/img/favicon/favicon-32x32.png',
                    rel: 'icon',
                    sizes: '32x32'
                },
                {
                    href: '/assets/img/favicon/favicon-16x16.png',
                    rel: 'icon',
                    sizes: '32x32'
                },
                {
                    href: 'https://fonts.googleapis.com/icon?family=Material+Icons',
                    rel: 'stylesheet'
                }
              ],
            title:'Adistec Intranet',
      // And any other config options from html-webpack-plugin:
      // https://github.com/ampedandwired/html-webpack-plugin#configuration
    }),
    extractCSS,
    extractSASS
    ]
};